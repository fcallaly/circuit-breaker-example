package com.example.circuitbreakerreading;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class BookService {

  @Value("${bookstore.url}")
  private String bookstoreUrl = "";
  private final RestTemplate restTemplate;

  public BookService(RestTemplate rest) {
	this.restTemplate = rest;
  }

  @HystrixCommand(fallbackMethod = "reliable")
  public String readingList() {
	URI uri = URI.create(bookstoreUrl + "/recommended");

	return this.restTemplate.getForObject(uri, String.class);
  }

  public String reliable() {
	return "Cloud Native Java (O'Reilly)";
  }

}
